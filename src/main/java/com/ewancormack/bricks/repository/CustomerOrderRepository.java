package com.ewancormack.bricks.repository;

import com.ewancormack.bricks.model.CustomerOrder;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Persistence for customer orders, extending JpaRepository means this interface has standard CRUD methods
 * For more info:
 * http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories
 */
public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, Long> {
}
