package com.ewancormack.bricks.service;

import com.ewancormack.bricks.model.CustomerOrder;

public interface CustomerOrderService {

    CustomerOrder save(CustomerOrder customerOrder);
}
