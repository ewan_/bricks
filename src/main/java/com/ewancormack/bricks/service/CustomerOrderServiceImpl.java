package com.ewancormack.bricks.service;

import com.ewancormack.bricks.model.CustomerOrder;
import com.ewancormack.bricks.repository.CustomerOrderRepository;
import org.springframework.stereotype.Service;

/**
 * Service for handling customer orders
 */
@Service
public class CustomerOrderServiceImpl implements CustomerOrderService {

    private final CustomerOrderRepository customerOrderRepository;

    public CustomerOrderServiceImpl(CustomerOrderRepository customerOrderRepository) {
        this.customerOrderRepository = customerOrderRepository;
    }

    @Override
    public CustomerOrder save(CustomerOrder customerOrder) {
        return customerOrderRepository.save(customerOrder);
    }
}
