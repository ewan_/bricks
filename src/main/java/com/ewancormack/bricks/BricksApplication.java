package com.ewancormack.bricks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Boiler plate to run a spring boot application
 */
@SpringBootApplication
public class BricksApplication {

    public static void main(String[] args) {
        SpringApplication.run(BricksApplication.class, args);
    }

}
