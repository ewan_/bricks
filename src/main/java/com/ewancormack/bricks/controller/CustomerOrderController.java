package com.ewancormack.bricks.controller;

import com.ewancormack.bricks.model.CustomerOrder;
import com.ewancormack.bricks.service.CustomerOrderService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Rest Controller for handling customer orders
 */
@RequestMapping("/v1/customer-order")
@RestController
public class CustomerOrderController {

    private final CustomerOrderService customerOrderService;

    public CustomerOrderController(CustomerOrderService customerOrderService) {
        this.customerOrderService = customerOrderService;
    }

    @PostMapping
    public CustomerOrder createCustomerOrder(@Valid @RequestBody CustomerOrder customerOrder) {
        return customerOrderService.save(customerOrder);
    }

}
