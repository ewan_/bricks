package com.ewancormack.bricks.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Domain object that represents an order of bricks, made by a customer
 */
@Entity
public class CustomerOrder {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private Long numberOfBricks;
    private boolean dispatched;

    public CustomerOrder() {

    }

    public CustomerOrder(Long id, Long numberOfBricks, boolean dispatched) {
        this.id = id;
        this.numberOfBricks = numberOfBricks;
        this.dispatched = dispatched;
    }

    public CustomerOrder(Long numberOfBricks) {
        this.numberOfBricks = numberOfBricks;
        this.dispatched = false;
    }

    public Long getId() {
        return id;
    }

    public Long getNumberOfBricks() {
        return numberOfBricks;
    }

    public boolean isDispatched() {
        return dispatched;
    }

    public void setNumberOfBricks(Long numberOfBricks) {
        this.numberOfBricks = numberOfBricks;
    }

    public CustomerOrder markAsDispatched() {
        this.dispatched = true;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerOrder that = (CustomerOrder) o;
        return dispatched == that.dispatched &&
                id.equals(that.id) &&
                numberOfBricks.equals(that.numberOfBricks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numberOfBricks, dispatched);
    }
}
