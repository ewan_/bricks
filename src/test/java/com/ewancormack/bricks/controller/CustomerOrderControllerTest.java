package com.ewancormack.bricks.controller;

import com.ewancormack.bricks.model.CustomerOrder;
import com.ewancormack.bricks.service.CustomerOrderService;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CustomerOrderControllerTest extends BaseControllerTest {

    @MockBean
    CustomerOrderService customerOrderService;

    @Test
    public void createOrder() throws Exception {

        CustomerOrder expected = new CustomerOrder(1L, 12L, false);
        when(customerOrderService.save(expected)).thenReturn(expected);

        mockMvc.perform(post(getRootUrl() + "/v1/customer-order")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(expected)))
                .andExpect(status().isOk())
                .andReturn();

        verify(customerOrderService).save(expected);
    }
}
